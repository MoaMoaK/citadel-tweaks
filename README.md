# Citadel Tweaks

This repository is a collection of tweaks that can be applied to
[Citadel](https://citadel.team). The main motivation behind this
project was to improve the website design.


## Dark theme

Adding a dark theme was the initial motivation for this repository.
The dark theme is simply a CSS file that contains a set of rules modifying
all the colors of the web interface.

The color scheme was choosen based on
[Microsoft Fluent Design](https://www.microsoft.com/design/fluent/) colors:

|               | Faded               | Standard            | Accentuated         |
| ------------- | ------------------- | ------------------- | ------------------- |
| Background    | #11100F (gray220)   | #201F1E (gray190)   | #323130 (gray160)   |
| Font          | #BEBBB8 (gray70)    | #EDEBE9 (gray40)    | #FAF9F8 (gray10)    |
| Neutral color |                     | #979593 (gray100)   | #797775 (gray120)   |
| Thematic blue | #00BCF2 (blueLight) | #0078D4 (blue)      | #00188F (blueMid)   | 


## Extrem responsiveness

Go further than the default responsiveness to minimize the size of different elements on
the page on small screens. In particular it reduces the _Rooms and People_ sidebar size.


## How to use custom CSS files

### Manual

* On __Firefox__: Open the _Developper Tools_ (F12), switch to the _Style Editor_ tab and add (+) a style sheet.


### Automatic

* On __Firefox__: Add this file to `%profile/chrome/userContent.css` (cf https://www.userchrome.org/). Use the `@-moz-document domain("thales.citadel.team")` selector.
* On __Chrome__: cf [this stackoverflow answer](https://stackoverflow.com/a/21210882)
* On __Edge__: Click on the red cros at the top right-hand corner and open Firefox.


### Plugins

* __Stylish__: for [Firefox](https://addons.mozilla.org/en-US/firefox/addon/stylish/), for [Chrome](https://chrome.google.com/webstore/detail/stylish-custom-themes-for/fjnbnpbmkenffdnngjfgmeleoegfcffe)
* __Stylus__: for [Firefox](https://addons.mozilla.org/en-US/firefox/addon/styl-us/), for [Chrome](https://chrome.google.com/webstore/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne/)


## Contributing

I do not pretend this project is top-notch quality (and I will never say so). Feel free to
improve the tweaks here by using merge requests. In particular, I'm no expert in design (the
choice of color was highly approximative) and far worse with CSS (this poject as a proof).
